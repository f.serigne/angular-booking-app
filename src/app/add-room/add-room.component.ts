import { Room, Type } from '../entities';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoomService } from '../room.service';
import { TypeService } from '../type.service';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.css']
})
export class AddRoomComponent implements OnInit {
types:Type[] =[];
  room:Room = {
    number: '',
    picture:'',
    price:0

  };
  constructor(private roService:RoomService, private router:Router, private ts:TypeService) { }

  ngOnInit(): void {
    this.roService.getAll().subscribe(data=> this.types=data);
  }

  addRoom() {
    this.roService.add(this.room).subscribe(() => {
      this.router.navigate(['/']);
    }); //On oublie pas le subscribe, sinon la requête ne sera pas lancée
  }
}
