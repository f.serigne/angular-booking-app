import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Room } from './entities';

@Injectable({
  providedIn: 'root'
})
export class RoomService {
  getRoomById(arg0: number) {
    throw new Error('Method not implemented.');
  }
  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Room[]>('/api/room');
  }

  getById(id:number) {
    return this.http.get<Room>('/api/room/'+id);
  }

  add(room:Room){
    return this.http.post<Room>('/api/room', room);
  }

  delete(id:number) {
    return this.http.delete('/api/room'+id);
  }

  put(room:Room){
    return this.http.put<Room>('/api/room'+room.id, room);
  }

  search(field:string) {
    // const params = new HttpParams()
    // .set('number', number);
    return this.http.get<Room[]>('/api/room/search/'+ field);
  }

  getRoomsByTypeId(id: number) {
    return this.http.get<Room[]>('/api/room/type/'+id);
  }

}

