import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking, Room } from '../entities';

import { switchMap } from 'rxjs';
import { RoomService } from '../room.service';
import { BookingService } from '../booking.service';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  laClasse = 'Selected';
  room:Room = {
    price: 0,
    number: '',
    picture:'',
    description:'',
  };
  booking:Booking={

    description:'',
    date:'',
   room:this.room


  }



  modalOpen:boolean = false;

  constructor(private roService:RoomService, private router:Router, private route: ActivatedRoute, private bs: BookingService) { }


  saveRoom() {
    this.bs.add(this.booking).subscribe();
    //appeler la foction save de l ordering service en lui passant order
    console.log(this.booking);
  }
  ngOnInit(): void {

    this.route.params.pipe(
      switchMap(params => this.roService.getById(params['id']))
    ).subscribe(data => {
      this.room = data,
    this.booking.room=data
    console.log(this.booking)
  }

    );


  }

  addRoom() {

    this.roService.add(this.room).subscribe(() => {
      this.router.navigate(['/']);
    })
  }

  toggleModal() {
    this.modalOpen = !this.modalOpen;

  }


}
