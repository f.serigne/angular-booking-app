import { TestBed } from '@angular/core/testing';

import { BookingLineService } from './booking-line.service';

describe('BookingLineService', () => {
  let service: BookingLineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BookingLineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
