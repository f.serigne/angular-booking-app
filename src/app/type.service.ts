import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Type } from './entities';

@Injectable({
  providedIn: 'root'
})
export class TypeService {
  getRoomsByTypeId(arg0: any) {
    throw new Error('Method not implemented.');
  }

  constructor(private http: HttpClient) { }
  getAll() {
    return this.http.get<Type[]>('/api/type');
  }

}
