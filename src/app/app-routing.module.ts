import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllUsersComponent } from './all-users/all-users.component';
import { AuthenticatedGuard } from './authentificated.guard';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { RoomComponent } from './room/room.component';
import { AdminRoomComponent } from './admin-room/admin-room.component';
import { AddRoomComponent } from './add-room/add-room.component';
import { SearchRoomComponent } from './search-room/search-room.component';
import { TypeComponent } from './type/type.component';
import { ModalComponent } from './modal/modal.component';
import { AdminComponent } from './admin/admin.component';
const routes: Routes = [
  {path:'admin', component: AdminComponent},
  {path:'', component:HomeComponent},
  {path:'login', component: LoginComponent},
  {path:'register', component: RegisterComponent},
  {path:'type', component: TypeComponent},
  {path:'all', component: AllUsersComponent},
  {path:'change-password', component: ChangePasswordComponent, canActivate: [AuthenticatedGuard]},
  {path:'room', component: RoomComponent},
  {path:'admin-room', component: AdminRoomComponent},
  {path:'add-room', component: AddRoomComponent},
  {path:'modal/:id', component: ModalComponent, canActivate: [AuthenticatedGuard]},
  {path:'search-room', component: SearchRoomComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
