import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name = '';

  email = '';
  password = '';
  passwordRepeat = '';
  hasError = false;

  constructor(private auth:AuthService, private router:Router) { }


  ngOnInit(): void {
  }


  register() {
    this.hasError = false;
    this.auth.register({name:this.name,email:this.email,password:this.password}).subscribe({
      next: data => this.router.navigate(['/']),
      error: () => this.hasError = true
    });

  }
}
