import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = '';
  password = '';
  passwordRepeat = '';
  hasError = false;
returnUrl='/';
  constructor(private auth:AuthService, private router:Router, private route: ActivatedRoute) { }


  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }


  login() {
    this.hasError = false;
    this.auth.login(this.email,this.password).subscribe({
      next: data => this.router.navigate([this.returnUrl]),
      error: () => this.hasError = true
    });

  }

}
