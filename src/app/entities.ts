
export interface Booking {
  id?: number;
  description?: string;
  date: string;
  room?: Room;
}

export interface User {
  id?: number;
  name: string;
  email: string;
  password: string;
  address?: string
  role?: string;
}

export interface Room {
  id?: number;
  number?: string;
  picture: string;
  description?: string
  price: number;
  type?: Type;
}
export interface Type {
  id?: number;
  name?: string;

}
