import { Component, OnInit } from '@angular/core';
import { Room } from '../entities';
import { RoomService } from '../room.service';

@Component({
  selector: 'app-search-room',
  templateUrl: './search-room.component.html',
  styleUrls: ['./search-room.component.css']
})
export class SearchRoomComponent implements OnInit {
room!: Room;
  // Attribut qui va faire le lien avec le champ de formulaire
  labelFilter?:string;
  // Liste d'opérations pour le retour de la requête
  list?:Room[] = [];
  // Booléen pour gérer l'affichage de la liste de résultats
  displayResultList:boolean = false;


  constructor(private roService:RoomService) {
    this.labelFilter = "";
   }

  ngOnInit(): void {
  }

  searchByNumber() {
    if (this.labelFilter?.length == 0) {
      alert("Merci de saisir une valeur !");
      document.getElementById('labelFilter')?.focus();
    } else {
      this.roService.search(this.labelFilter!).subscribe(
        data => this.list = data
      );
      this.displayResultList = true;
    }
  }
}
