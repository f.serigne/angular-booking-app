import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoomService } from '../room.service';
import { Room } from '../entities';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.css']
})
export class SingleComponent implements OnInit {

  constructor(private route:ActivatedRoute, private roService:RoomService) { }
room?: Room;
routedId?:string;
  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.routedId = param['id'];
      console.log(this.routedId);
      this.roService.getById(Number(this.routedId!)).subscribe(data=>this.room=data);


    });

  }
  }


