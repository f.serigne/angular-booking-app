import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{

  showRooms: any;
field='';
title = 'angular-auth';
authState = this.auth.state;
constructor(private auth:AuthService,private router:Router) {}

logout() {
  this.auth.logout().subscribe(()=> this.router.navigate(["/login"]));
}
}
