import { Component, OnInit } from '@angular/core';
import { Room } from '../entities';
import { RoomService } from '../room.service';
@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})

  export class RoomComponent implements OnInit {

      rooms:Room[] = [];
      single?: Room;
      displayEditForm: boolean = false;
      modifiedRo: Room = {
        number: '',
        description: '',
        picture:'',
        price:0
      }
      typeFilter?:string;
      // categoryId?: number;



      constructor(private rs : RoomService) { }

      ngOnInit(): void {
        this.rs.getAll().subscribe(data => this.rooms = data);
      }

      fetchOne() {
        this.rs.getById(2);
      }

      fetchAll() {
        this.rs.getAll().subscribe(data => this.rooms = data);
      }

      delete(id:number) {
        this.rs.delete(id).subscribe();
        this.removeFromList(id);
      }

      removeFromList(id: number) {
        this.rooms.forEach((room, room_index) => {
          if(room.id == id) {
            this.rooms.splice(room_index, 1);
          }
        });
      }

      showEditForm(room:Room) {
        this.displayEditForm = true;
        this.modifiedRo = Object.assign({},room);
      }

      update(room:Room) {

        this.rs.put(room).subscribe();
        this.updateRoomInList(room);
        this.displayEditForm = false;
      }

      updateRoomInList(room:Room) {
        this.rooms.forEach((pr_list) => {
          if(pr_list.id == room.id) {
            pr_list.number = room.number;
            pr_list.description = room.description;
            pr_list.price = room.price;
          }
        });
      }

    }
