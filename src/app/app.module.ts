import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {ReactiveFormsModule} from '@angular/forms';
import { HomeComponent } from './home/home.component';

import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { AllUsersComponent } from './all-users/all-users.component';
import { CredentialInterceptor } from './credential.interceptor';

import { ChangePasswordComponent } from './change-password/change-password.component';
import { LoginComponent } from './login/login.component';
import { AdminRoomComponent } from './admin-room/admin-room.component';
import { RoomComponent } from './room/room.component';
import { HeaderComponent } from './header/header.component';
import { AddRoomComponent } from './add-room/add-room.component';
import { SearchRoomComponent } from './search-room/search-room.component';
import { ModalComponent } from './modal/modal.component';
import { TypeComponent } from './type/type.component';
import { SingleComponent } from './single/single.component';
import { AdminComponent } from './admin/admin.component';






@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
     AllUsersComponent,
     ChangePasswordComponent,
     AdminRoomComponent,
     RoomComponent,
     HeaderComponent,
     AddRoomComponent,
     SearchRoomComponent,
     ModalComponent,
     TypeComponent,
     SingleComponent,
     AdminComponent,




 ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [
    {provide:HTTP_INTERCEPTORS,useClass:CredentialInterceptor,multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
