import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { User, Room } from '../entities';
import { RoomService } from '../room.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  list:Room[] = [];
  single?:Room;
  displayEditForm:boolean = false;
  modifiedRo:Room = {
    picture:'',
    price:0
  }

  constructor(private roService:RoomService,private router:Router) { }

  add(id:number) {
    this.router.navigateByUrl('/modal/'+id);
  }
  ngOnInit(): void {
    this.roService.getAll().subscribe(data => {this.list = data
      console.log(this.list)});

  }

  fetchOne() {
    this.roService.getById(2).subscribe(data => this.single = data);
  }

  delete(id:number) {
    this.roService.delete(id).subscribe();
    this.removeFromList(id);
  }

  removeFromList(id: number) {
    this.list.forEach((room, room_index) => {
      if(room.id == id) {
        this.list.splice(room_index, 1);
      }
    });
  }

  fetchAll(){
    this.roService.getAll().subscribe(data => this.list = data);
  }

  showEditForm(room:Room) {
    this.displayEditForm = true;
    // Impossible, le = copie juste la référence, il ne fait pas une copie
    // this.modifiedOp = room;
    this.modifiedRo = Object.assign({},room);
  }

  update(room:Room) {
    // Envoyer au serveur
    this.roService.put(room).subscribe();
    // Mettre à jour la liste en HTML
    this.updateRoomInList(room);
    // Fermer le formulaire
    this.displayEditForm = false;
  }

  updateRoomInList(room:Room) {
    this.list.forEach((ro_list) => {
      if(ro_list.id == room.id) {
        ro_list.number = room.number;
        ro_list.description = room.description;
        ro_list.price = room.price;
      }
    });
  }

}
