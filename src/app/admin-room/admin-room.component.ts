import { Component, OnInit } from '@angular/core';
import { Room } from '../entities';
import { RoomService } from '../room.service';

@Component({
  selector: 'app-admin-room',
  templateUrl: './admin-room.component.html',
  styleUrls: ['./admin-room.component.css']
})
export class AdminRoomComponent implements OnInit {

  rooms? = '';


  displayEditForm: boolean = false;
  modifiedRo: Room = {
    id: 0,
    number: '',
    picture:'',
    description: '',
    price: 0
  };
  list: any;

  constructor(private rService: RoomService) { }

  ngOnInit(): void {
    this.rService.getAll().subscribe((data: any) => this.list.rooms = data);
  }

  delete(id:number) {
    this.rService.delete(id);
    this.removeFromList(id);
  }
  removeFromList(id: number) {
    this.list.rooms?.forEach((room: { id: number; }, room_index: any) => {
      if (room.id == id) {
        this.rooms?.slice(room_index, 1);
      }
    });
  }

  showEditForm(room:Room) {
    this.displayEditForm = true;
    this.modifiedRo = Object.assign({}, room)
  }

  update(room:Room) {
    this.rService.put(room).subscribe();
    this.updateRoomInList(room);
    this.displayEditForm = false;

  }

  updateRoomInList(room:Room) {
    this.list.rooms?.forEach((r_list: { id: number; number: string; description: string; price: number}) => {
      if (r_list.id == room.id) {
        r_list.number == room.number;
        r_list.description == room.description;
        r_list.price = room.price;

      }
    });

  }

}



