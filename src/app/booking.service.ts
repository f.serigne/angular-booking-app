
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Booking, Room } from './entities';

@Injectable({
  providedIn: 'root'
})
export class BookingService {


  constructor(private http: HttpClient) { }

  add(booking:Booking){
    return this.http.post<Booking>('/api/booking', booking);
  }

}


