import { Room, Type } from '../entities';
import { Component, OnInit } from '@angular/core';
import { TypeService } from '../type.service';
import { RoomService } from '../room.service';

@Component({
  selector: 'app-type',
  templateUrl: './type.component.html',
  styleUrls: ['./type.component.css']
})
export class TypeComponent implements OnInit {

types?:Type[];
selectedType?:Type;
rooms?: Room[];

  constructor(private ts: TypeService, private roService: RoomService) { }



  ngOnInit(): void {

    this.ts.getAll().subscribe(data => this.types=data);
  }

  fetchRooms() {
    if(this.selectedType){
      this.roService.getRoomsByTypeId(this.selectedType.id!).subscribe(data=>{
        this.rooms=data,
      console.log(this.rooms);
      });
    }

  }

}
